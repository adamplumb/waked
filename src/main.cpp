/*
Waked
Copyright (C) 2021  Robin Westermann <waked@seath.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <sdbus-c++/sdbus-c++.h>
#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <getopt.h>

#include "alarm.h"

const char* autoWakeupId = "auto-wakeup";
const char* initializedAlarmId = "initialized-alarm-id";

const char* inhibitorDestinationName = "org.freedesktop.login1";
const char* inhibitorObjectPath = "/org/freedesktop/login1";
const char* inhibitorInterfaceName = "org.freedesktop.login1.Manager";
std::unique_ptr inhibitorProxy = sdbus::createProxy(inhibitorDestinationName, inhibitorObjectPath);
std::list<Alarm> alarmList;
sdbus::UnixFd suspendDelayLockFd;

std::uint64_t autoWakeupInterval = 0UL;

void writeToRTC(std::uint64_t data) {
    std::cout << "Writing to RTC: " << data << std::endl;
    std::ofstream rtc("/sys/class/rtc/rtc0/wakealarm");
    if (rtc.is_open()) {
        rtc << data << std::endl;
    } else {
        std::cout << "ERROR: Couldn't open RTC to write" << std::endl;
    }
}

std::uint64_t readFromRTC() {
    std::cout << "Reading from RTC: ... ";
    std::string line;
    std::ifstream rtc;
    rtc.open ("/sys/class/rtc/rtc0/wakealarm");
    rtc >> line;
    rtc.close();
    std::cout << line << "=";
    std::uint64_t data = 0;
    if (line.size()) {
        data = std::stoul(line);
    }
    std::cout << data << std::endl;
    return data;
}

void addAlarmToList(const std::string& id, const std::uint64_t alarmTime)
{
    std::time_t now = std::time(nullptr);
    std::cout << "Adding alarm: id=" << id << " time="<< alarmTime << " in " << alarmTime - now << " secs" <<  std::endl;

    Alarm newAlarm(id,alarmTime);
    alarmList.push_back(std::move(newAlarm));
    alarmList.sort([](Alarm &a, Alarm &b) {
                       return a.getTime() < b.getTime();
                   });
}

void removeAlarmFromList(const std::string& id)
{
    std::cout << "remove alarm: id=" << id << std::endl;
    if ((alarmList.size())
            && (alarmList.front().getId() == id)
            && (alarmList.front().getTime() == readFromRTC())) {
        writeToRTC(0);
    }

    alarmList.remove_if([id](Alarm &a){return id == a.getId();});
}

void reschedule()
{
    std::cout << "Reschedule..." << std::endl;
    std::time_t now = std::time(nullptr);
    alarmList.remove_if([now](Alarm &a){ return a.getTime() < now || a.getId() == autoWakeupId; });

    // Ensure there is always a wakeup in some minutes
    // If there is no alarm scheduled, this schedules one for auto-wakeup
    if (autoWakeupInterval > 0) {
        std:uint64_t autoWakeTime = now + autoWakeupInterval;
        addAlarmToList(autoWakeupId, autoWakeTime);
   } 

    if (alarmList.size()) {
        Alarm nextAlarm = alarmList.front();
        std::string id = nextAlarm.getId();
        std::uint64_t alarmTime = nextAlarm.getTime();
        
        uint64_t localReadFromRTC = readFromRTC();
        if ((alarmTime < localReadFromRTC) || (!localReadFromRTC)) {
            writeToRTC(0);
            
            std::cout << "Scheduling next alarm for id " << id << " in " << alarmTime - now << " secs" << std::endl; 
            writeToRTC(alarmTime);
        }
    }
}

/**
 * Read in whatever is set in the RTC so we can survive a service restart
 */
void initializeAlarms()
{
    if (alarmList.size() > 0) {
        std::cout << "Alarms are already present, not initializing alarms" << std::endl;
        return;
    }

    std::cout << "Initializing alarms" << std::endl;

    uint64_t localReadFromRTC = readFromRTC();
    std::time_t now = std::time(nullptr);

    if (localReadFromRTC && localReadFromRTC > now) {
        std::cout << "Found RTC alarm set for " << localReadFromRTC - now << " secs from now. Adding as local alarm" << std::endl;
        addAlarmToList(initializedAlarmId, localReadFromRTC);
    }
}

std::string registerAlarm(const std::string& id, const std::uint64_t alarmTime)
{
    addAlarmToList(id, alarmTime);
    reschedule();

    return "ok";
}

std::string deregisterAlarm(const std::string& id)
{
    removeAlarmFromList(id);
    reschedule();

    return "ok";
}

std::string enableAutoWakeup(std::uint64_t interval)
{
    if (interval < 60) {
        interval = 60;
    }

    std::cout << "Enabling auto-wakeup with interval " << interval << std::endl;

    autoWakeupInterval = interval;
    reschedule();

    return "ok";
}

std::string disableAutoWakeup()
{
    std::cout << "Disabling auto-wakeup interval" << std::endl;

    autoWakeupInterval = 0;
    reschedule();

    return "ok";
}

std::string updateAlarm(const std::string& id, const std::uint64_t alarmTime)
{
    std::cout << "update alarm: id=" << id << " time="<< alarmTime << std::endl;
    deregisterAlarm(id);
    registerAlarm(id, alarmTime);

    return "ok";
}

void handleSuspend(const bool active) {
    if (active) {
        std::cout << "Checking alarm for suspend ..." << std::endl;
        reschedule();

        // Delay wake-up if alarm is near
        std::time_t now = std::time(nullptr);
        if ((alarmList.size()) && (alarmList.front().getTime() < now + 10UL)) {
            std::cout << "Next alarm too close. Wake up in 10 Seconds ..." << std::endl;
            writeToRTC(now + 10UL);
        }
        suspendDelayLockFd.reset();
    } else {
        inhibitorProxy->callMethod("Inhibit").onInterface(inhibitorInterfaceName).withArguments("sleep", "Waked", "Manage system wake-ups", "delay").storeResultsTo(suspendDelayLockFd);
        std::cout << "Woke up from suspend ..." << std::endl;
        reschedule();
    }
}

int main(int argc, char *argv[])
{
    int opt;
    while ((opt = getopt(argc, argv, "w:h")) != -1) {
        switch (opt) {
            case 'w':
                if (optarg[0] == '-') {
                    std::cerr << "Auto-wakeup interval must be a positive number" << std::endl;
                    return 1;
                }

                autoWakeupInterval = strtoul(optarg, NULL, 0);
                std::cout << "Setting auto-wakeup interval to " << autoWakeupInterval << " seconds" << std::endl;

                if (autoWakeupInterval < 60) {
                    std::cerr << "Auto-wakeup interval must be greater than 60 seconds" << std::endl;
                    return 1;
                }
                break;

            case 'h':
                std::cout << "Usage: waked [-w AUTO_WAKEUP_INTERVAL_SECS]" << std::endl;
                return 0;
        }
    }

    // Create D-Bus connection to the system bus and requests name on it.
    const char* serviceName = "de.seath.Waked";
    auto connection = sdbus::createSystemBusConnection(serviceName);

    const char* wakedObjectPath = "/de/seath/Waked/Alarm";
    auto wakedDbusObject = sdbus::createObject(*connection, wakedObjectPath);

    const char* wakedInterfaceName = "de.seath.Waked";
    wakedDbusObject->registerMethod("Add").onInterface(wakedInterfaceName).implementedAs(&registerAlarm);
    wakedDbusObject->registerMethod("Update").onInterface(wakedInterfaceName).implementedAs(&updateAlarm);
    wakedDbusObject->registerMethod("Remove").onInterface(wakedInterfaceName).implementedAs(&deregisterAlarm);
    wakedDbusObject->registerMethod("EnableAutoWakeup").onInterface(wakedInterfaceName).implementedAs(&enableAutoWakeup);
    wakedDbusObject->registerMethod("DisableAutoWakeup").onInterface(wakedInterfaceName).implementedAs(&disableAutoWakeup);
    wakedDbusObject->finishRegistration();
    
    inhibitorProxy->uponSignal("PrepareForSleep").onInterface(inhibitorInterfaceName).call(&handleSuspend);  //[](const std::string& str){ onConcatenated(str); });
    inhibitorProxy->finishRegistration();

    inhibitorProxy->callMethod("Inhibit").onInterface(inhibitorInterfaceName).withArguments("sleep", "Waked", "Manage system wake-ups", "delay").storeResultsTo(suspendDelayLockFd);

    initializeAlarms();
    reschedule();

    // Run the loop on the connection.
    connection->enterEventLoop();
}

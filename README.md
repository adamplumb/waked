# Waked
Waked is a daemon which lets Apps wake the system from suspend at requested times

# Install
There is a PKGBUILD here: https://gitlab.com/seath1/pkgbuild/-/tree/master/waked
After installing start/enable the service:
```
systemctl enable waked
systemctl start waked
```

# Dependencies
 - sdbus-cpp
 - the usual stuff to build things (cmake, g++, make, ...)

# Usage
The service listens on the system bus at
```
service:   de.seath.Waked
interface: de.seath.Waked
object:    /de/seath/Waked/Alarm
```
Methods:
```
Add (string id, uint64 time)
Update (string id, uint64 time)
Remove (string id)
EnableAutoWakeup (uint64 intervalSecs)
DisableAutoWakeup ()
```

To add/change/remove an alarm use the corrosponding method via dbus.
 - id can be freely chosen.
 - time is the timestamp to wake up the system in unix time (seconds since epoch)

# Auto Wakeup
The auto-wakeup mechanism allows you to set an interval that forces the phone to wake up every X number of seconds.
This can be useful when you want to regularly check for notifications from apps that aren't able to wake up the phone themselves, such as Matrix @s and emails.  It is best to
pair this with a short idle sleep time, like 1 minute, to prevent battery draining.  An interval of between 15-30 minutes (900-1800 seconds) seems like a good start. The minimum interval is 60 seconds.

To enable auto-wakeup from the command line, pass `-w [SECS]` when starting waked.  You can also dynamically set the auto-wakeup interval through the dbus interface

# State
 atm just a proof of concept to use a pinephone as alarm clock with a modified gnome-clocks version you can find here: https://gitlab.com/seath1/gnome-clocks-waked

 The following features may or may not be added in the future:
 - use kernel timers or systemd timers or something completly different instead of writing to the rtc
 - persist registered timers between reboots and restarting the service.
 - concept to allow/deny specific Apps
 - change the API without further notice
 - your idea?
